<?php

include( __DIR__ . '/vendor/autoload.php' );
include( __DIR__ . '/config.php' );

use OpenCloud\Rackspace;


function boxcar_notify($source_name, $title, $message, $sound="bird-1", $icon=null){
	global $BOXCAR_KEY;

	curl_setopt_array(
		$chpush = curl_init(),
		array(
			CURLOPT_URL => "https://new.boxcar.io/api/notifications",
			CURLOPT_POSTFIELDS => array(
				"user_credentials" => $BOXCAR_KEY,
				"notification[title]" => $title,
				"notification[long_message]" => $message,
				"notification[sound]" => $sound,
				"notification[source_name]" => $source_name,
				"notification[icon_url]" => $icon
			)
		)
	);

	$ret = curl_exec($chpush);
	curl_close($chpush);
}


$ip = trim( file_get_contents( $IP_URL ) );

$ip_store = __DIR__ . '/tmp/ip';
$current_ip = file_exists( $ip_store ) ? trim( file_get_contents( $ip_store ) ) : false;

print "IP: $ip\n";
print "Stored IP: $current_ip\n";

if(!$ip || strlen($ip) == 0) exit;
if($ip == $current_ip){
	print "No update required\n";
	exit;
}

print "Setting IP to: $ip\n";

$client = new Rackspace($API_ENDPOINT, array(
	'username' => $RACKSPACE_USERNAME,
	'apiKey'   => $RACKSPACE_API_KEY,
));

$dns = $client->dnsService(null, $REGION);

$domains = $dns->domainList(array(
	'name' => $DOMAIN,
	'type' => 'A'
));

foreach ($domains as $domain) {
	$domainId = $domain->id;
	break;
}

$domain = $dns->domain($domainId);

$records = $domain->recordList(array(
    'name' => $SUB_DOMAIN,
    'type' => 'A',
));

foreach ($records as $record) {
	$recordId = $record->id;
	break;
}

$record = $domain->record($recordId);

$record->update(array(
	'data' => $ip,
));

print "Done\n";

if($record) file_put_contents( $ip_store, $ip );

boxcar_notify("Dynamic DNS", "IP address updated", "$SUB_DOMAIN > $ip", $BOXCAR_INFO_SOUND, $BOXCAR_INFO);
